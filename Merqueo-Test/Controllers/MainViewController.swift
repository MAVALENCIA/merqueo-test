//
//  MainViewController.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/28/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import UIKit

enum filterType: Int {
	case storeName = 0
	case category
	case delivery
	case rating
	case deliveryTime
}

class MainViewController: UIViewController {
	
	@IBOutlet weak var lblTitleView: 	UILabel!
	
	@IBOutlet weak var storeTableView: 	UITableView!
	
	var stores: 		[Store]?
	
	var storeArraySize: Int?
	var storeNameRef:	String?
	
	var pickerFilter	= SBPickerSelector()
	var pickerOption: 	filterType?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initPickers()
		self.storeTableView.separatorStyle = .none
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	

	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		// Get the new view controller using segue.destinationViewController.
		// Pass the selected object to the new view controller.
	}
	*/
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "StoreDetailSegue" {
			let StoreDetailVC	= segue.destination as! StoreDetailViewController
			StoreDetailVC.storeNameRef = self.storeNameRef
		}
	}
	
	@IBAction func goBack(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func filterOptions(_ sender: UIBarButtonItem) {
		self.pickerFilter.showPickerOver(self)
	}

}

extension MainViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.storeArraySize! != -1 ? self.storeArraySize! : 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreViewCell
		cell.starImage_1.image = UIImage(named: "Emptystar")
		cell.starImage_2.image = UIImage(named: "Emptystar")
		cell.starImage_3.image = UIImage(named: "Emptystar")
		cell.starImage_4.image = UIImage(named: "Emptystar")
		cell.starImage_5.image = UIImage(named: "Emptystar")
		
		cell.lblStoreName.text 			= self.stores![indexPath.row].store_name
		cell.lblStoreCategory.text 		= self.stores![indexPath.row].category
		cell.lblDeliveryTimeValue.text	= "\(self.stores![indexPath.row].delivery_time) min"
		cell.lblDeliveryCost.text 		= "$\((self.stores![indexPath.row].delivery).decimalFormatted())"
		
		switch self.stores![indexPath.row].rating {
		case 1:
			cell.starImage_1.image = UIImage(named: "Fullstar")
		case 2:
			cell.starImage_1.image = UIImage(named: "Fullstar")
			cell.starImage_2.image = UIImage(named: "Fullstar")
		case 3:
			cell.starImage_1.image = UIImage(named: "Fullstar")
			cell.starImage_2.image = UIImage(named: "Fullstar")
			cell.starImage_3.image = UIImage(named: "Fullstar")
		case 4:
			cell.starImage_1.image = UIImage(named: "Fullstar")
			cell.starImage_2.image = UIImage(named: "Fullstar")
			cell.starImage_3.image = UIImage(named: "Fullstar")
			cell.starImage_4.image = UIImage(named: "Fullstar")
		case 5:
			cell.starImage_1.image = UIImage(named: "Fullstar")
			cell.starImage_2.image = UIImage(named: "Fullstar")
			cell.starImage_3.image = UIImage(named: "Fullstar")
			cell.starImage_4.image = UIImage(named: "Fullstar")
			cell.starImage_5.image = UIImage(named: "Fullstar")
		default:
			print("No entró en ninguna opción")
		}
		
		return cell
	}
}

extension MainViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.storeNameRef = self.stores?[indexPath.row].store_name
		self.performSegue(withIdentifier: "StoreDetailSegue", sender: nil)
	}
}

extension MainViewController: SBPickerSelectorDelegate {
	
	func initPickers() {
		self.pickerFilter.pickerData.append("Nombre de restaurante")
		self.pickerFilter.pickerData.append("Categoría")
		self.pickerFilter.pickerData.append("Valor de domicilio")
		self.pickerFilter.pickerData.append("Rating")
		self.pickerFilter.pickerData.append("Tiempo de domicilio")
		
		self.pickerFilter.delegate		= self
		self.pickerFilter.pickerType	= .text
	}
	
	func pickerSelector(_ selector: SBPickerSelector, selectedValue value: String, index idx: Int) {
		if selector === self.pickerFilter {
			
			switch  filterType(rawValue: idx)! {
			case .category:
				self.stores!.sort(by: {$0.category < $1.category})
				break
				
			case .delivery:
				self.stores!.sort(by: {$0.delivery < $1.delivery})
				break
				
			case .deliveryTime:
				self.stores!.sort(by: {$0.delivery_time < $1.delivery_time})
				break
				
			case .rating:
				self.stores!.sort(by: {$0.rating > $1.rating})
				break
				
			default:
				self.stores!.sort(by: {$0.store_name < $1.store_name})
				break
			}
			
			self.storeTableView.reloadData()
		}
	}
}


class StoreViewCell: UITableViewCell {
	
	@IBOutlet weak var lblStoreName: 			UILabel!
	@IBOutlet weak var lblStoreCategory: 		UILabel!
	@IBOutlet weak var lblDeliveryTime: 		UILabel!
	@IBOutlet weak var lblDeliveryTimeValue:	UILabel!
	@IBOutlet weak var lblDelivery: 			UILabel!
	@IBOutlet weak var lblDeliveryCost: 		UILabel!
	
	@IBOutlet weak var storeImage: 				UIImageView!
	@IBOutlet weak var starImage_1: 			UIImageView!
	@IBOutlet weak var starImage_2: 			UIImageView!
	@IBOutlet weak var starImage_3: 			UIImageView!
	@IBOutlet weak var starImage_4: 			UIImageView!
	@IBOutlet weak var starImage_5: 			UIImageView!
	
	
//	override func awakeFromNib() {
//		super.awakeFromNib()
//		// Initialization code
//	}
	
//	override func setSelected(_ selected: Bool, animated: Bool) {
//		super.setSelected(selected, animated: animated)
//	}
}
