//
//  LogsRecord.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/29/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import UIKit
import Foundation

struct logKey {
	static let code			= "code"
	static let desc			= "desc"
	static let origin		= "origin"
	static let timestamp	= "timestamp"
	static let systeminfo	= "systeminfo"
	static let userinfo		= "userinfo"
}

class Log: NSObject, NSCoding {
	let code: Int?
	let desc: String?
	let origin: String?
	let timestamp: String?
	let systeminfo: String?
	let userinfo: String?
	
	init(code: Int!, desc: String!, origin: String!, userinfo: String?) {
		self.code 		= code!
		self.desc 		= desc!
		self.origin 	= origin!
		self.timestamp 	= Date().formatToServerDate()
		self.systeminfo	= "\(UIDevice().type.rawValue) (iOS \(UIDevice.current.systemVersion))"
		self.userinfo 	= userinfo
	}
	
	required init(coder decoder: NSCoder) {
		self.code 		= decoder.decodeInteger(forKey: logKey.code)
		self.desc 		= decoder.decodeObject(forKey: logKey.desc) as? String ?? ""
		self.origin 	= decoder.decodeObject(forKey: logKey.origin) as? String ?? ""
		self.timestamp 	= decoder.decodeObject(forKey: logKey.timestamp) as? String ?? ""
		self.systeminfo	= decoder.decodeObject(forKey: logKey.systeminfo) as? String ?? ""
		self.userinfo 	= decoder.decodeObject(forKey: logKey.userinfo) as? String ?? ""
	}
	
	func encode(with aCoder: NSCoder) {
		aCoder.encode(code!, forKey: logKey.code)
		aCoder.encode(desc!, forKey: logKey.desc)
		aCoder.encode(origin!, forKey: logKey.origin)
		aCoder.encode(timestamp!, forKey: logKey.timestamp)
		aCoder.encode(systeminfo!, forKey: logKey.systeminfo)
		aCoder.encode(userinfo, forKey: logKey.userinfo)
	}
}

class LogsRecord {
	
	var granted: Bool = false
	var logs: [Log]!
	
	init() {
		self.granted = UserDefaults.standard.bool(forKey: "LOG-RECORD")
		if let data = UserDefaults.standard.data(forKey: "logs") {
			if let mlogs = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Log] {
				self.logs = mlogs
			}else{
				self.logs = [Log]()
			}
		} else {
			self.logs = [Log]()
		}
	}
	
	func add(_ log: Log){
		if granted {
			self.logs.append(log)
			let encodedData = NSKeyedArchiver.archivedData(withRootObject: logs)
			UserDefaults.standard.set(encodedData, forKey: "logs")
		}else{
			print("Permission for log record is denied")
		}
	}
	
	func update(with logs: [Log]!) {
		if granted {
			let encodedData = NSKeyedArchiver.archivedData(withRootObject: logs)
			UserDefaults.standard.set(encodedData, forKey: "logs")
		}else{
			print("Permission for log record is denied")
		}
	}
	
	func clean() {
		if !logs.isEmpty {
			UserDefaults.standard.removeObject(forKey: "logs")
		}else{
			print("Permission for log record is denied")
		}
	}
	
	func setPermissionForLog(granted: Bool) {
		UserDefaults.standard.set(granted, forKey: "LOG-RECORD")
	}
	
	func sendToServer() {
		if granted {
			let log		= logs.last!
			let values	= "code=\(log.code!)&desc=\(log.desc!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)&origin=\(log.origin!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)&timestamp=\(log.timestamp!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)&systeminfo=\(log.systeminfo!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)&userinfo=\(log.userinfo!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)"
			
			Request().expectingObject(endpoint: .logErrors, method: "POST", params: values, token: nil,
				  completion: { data, code, err in
					if err != nil {
						print("There was an error sending log with code: \(log.code!) and timestamp: \(log.timestamp!)")
						print(err!.debugDescription)
					}else{
						if code != 200 {
							print("There was an error in response trying sending log with code: \(log.code!) and timestamp: \(log.timestamp!)")
						}else{
							print("Log with code: \(log.code!) and timestamp: \(log.timestamp!) sent successfully!")
							//print(data.debugDescription)
							self.logs!.removeLast()
							self.update(with: self.logs!)
							if !self.logs!.isEmpty {
								self.sendToServer()
							}
						}
					}
				}
			)
		}else{
			print("Permission for log record is denied")
		}
	}
	
}
