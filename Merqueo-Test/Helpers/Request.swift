//
//  Request.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/29/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import Foundation
import UIKit

enum Endpoints:	String {
	
	case stores	= "/bins/1zib8"
	case logErrors = "/route_reference/errors_log"
	
}

class Request {
	
	let host:	String = "https://api.myjson.com"
	var config:	URLSessionConfiguration {
		get {
			let sesConfig = URLSessionConfiguration.default
			sesConfig.timeoutIntervalForRequest = 20
			sesConfig.timeoutIntervalForResource = 60
			return sesConfig
		}
	}
	
	func expectingObject(endpoint: Endpoints!, method: String!, params: String!, token: String?, completion: @escaping (_ data: NSDictionary?,_ code: Int?, _ err: String?) -> Void) {
		
		if InternetAccess().isConnectedToNetwork() {
			
			var url: NSURL!
			url = NSURL(string: "\(host)\(endpoint.rawValue)")!
			print("\(url as! String)")
			
			let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
			request.httpMethod = method
			
			let postData:Data = params.data(using: .ascii)!
			let postLength = String( postData.count )
			request.httpBody = postData
			request.setValue(postLength, forHTTPHeaderField: "Content-Length")
			request.setValue("application/json", forHTTPHeaderField: "Accept")
			if token != nil {
				request.setValue( "\(token!)", forHTTPHeaderField: "x-auth-token")
			}
			let task = URLSession(configuration: config).dataTask(with: request as URLRequest) { data, response, error in
				
				if(error != nil){
					print(error!.localizedDescription)
					DispatchQueue.main.async {
						completion(nil, 1, error?.localizedDescription)
					}
				}else{
					let HttpResp = response as! HTTPURLResponse
					let sCode = HttpResp.statusCode
					let dataString = String(data: data!, encoding: .utf8)
					do {
						let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSObject
						if let results = jsonResult as? NSDictionary {
							DispatchQueue.main.async {
								//                                print(results)
								completion(results, sCode, nil)
							}
						}else{
							completion(nil, 2, "Malformed response")
						}
					} catch let error as NSError {
						DispatchQueue.main.async {
							completion(nil, sCode, error.localizedDescription)
						}
					}
					
				}
			}
			task.resume()
		}else{
			completion(nil, 3, "No hay conexión a internet")
		}
	}

	func expectingObjects(endpoint: Endpoints!, method: String!, params: String!, token: String?, completion: @escaping (_ data: [NSDictionary]?,_ code: Int?, _ err: String?) -> Void) {
			
		if InternetAccess().isConnectedToNetwork() {
			let url:NSURL = NSURL(string: "\(host)\(endpoint.rawValue)")!
			let request:NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
			request.httpMethod = method
			
			let postData:Data = params.data(using: .ascii)!
			let postLength = String( postData.count )
			request.httpBody = postData
			request.setValue(postLength, forHTTPHeaderField: "Content-Length")
			request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
			request.setValue("application/json", forHTTPHeaderField: "Accept")
			if token != nil {
				request.setValue( "\(token!)", forHTTPHeaderField: "x-auth-token")
			}
			let task = URLSession(configuration: config).dataTask(with: request as URLRequest) { data, response, error in
				
				if(error != nil){
					print(error!.localizedDescription)
					DispatchQueue.main.async {
						completion(nil, 1, error?.localizedDescription)
					}
				}else{
					let HttpResp = response as! HTTPURLResponse
					let sCode = HttpResp.statusCode
					
					do {
						let jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSObject
						if let results = jsonResult as? [NSDictionary] {
							DispatchQueue.main.async {
								completion(results, sCode, nil)
							}
						}else{
							completion(nil, 2, "Malformed response")
						}
					} catch let error as NSError {
						DispatchQueue.main.async {
							completion(nil, sCode, error.localizedDescription)
						}
					}
				}
			}
			task.resume()
		}else{
			completion(nil, 3, "No hay conexión a internet")
		}
	}
}
