//
//  Store.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/28/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import UIKit

struct Store {
	var category:		String
	var delivery:		Int
	var url_detail:		String
	var logo_path:		String
	var store_name:		String
	var rating:			Int
	var delivery_time:	Int
	var lat:			String
	var long:			String
}
	// Reference Dictionary
	// "categorias": "Hamburguesas y Perros Calientes",
	// "domicilio": "4300.00",
	// "url_detalle": "",
	// "logo_path": "http://clic-1032-api-promos-first-time.web.development.2delivery.co/timthumb?src=http%3A%2F%2Fclic-1032-api-promos-first-time.web.development.2delivery.co%2Fimg%2Fresources%2F12105.jpg&q=70&a=c&zc=2&ct=1&w=120&h=120",
	// "nombre": "Presto",
	// "rating": "3",
	// "tiempo_domicilio": "45",
	// "ubicacion_txt": ""
