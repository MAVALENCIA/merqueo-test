//
//  StoreDetailViewController.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/28/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import UIKit

class StoreDetailViewController: UIViewController {

	@IBOutlet weak var storeName: 		UILabel!
	
	@IBOutlet weak var menuTableView: 	UITableView!
	
	let arrayMenu:		[String] = ["Hamburguesas", "Perros calientes", "Salchipapas", "Chuzos degranados"]
	var storeNameRef:	String?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.storeName.text = self.storeNameRef
		self.menuTableView.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	@IBAction func goBack(_ sender: UIBarButtonItem) {
		self.navigationController?.popViewController(animated: true)
	}
	

}

extension StoreDetailViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.arrayMenu.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
		cell.foodCategory.text = self.arrayMenu[indexPath.row]
		return cell
	}
	
}

extension StoreDetailViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
	}
}

class MenuCell: UITableViewCell {
	
	@IBOutlet weak var foodCategory: UILabel!

	//	override func awakeFromNib() {
		//		super.awakeFromNib()
	//	}
	
}
