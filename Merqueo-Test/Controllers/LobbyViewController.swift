//
//  LobbyViewController.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/28/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class LobbyViewController: UIViewController {

	@IBOutlet weak var lblTitleView: 		UILabel!
	@IBOutlet weak var lblCityReference: 	UILabel!
	@IBOutlet weak var lblCountryReference: UILabel!
	
	@IBOutlet weak var btnShowStores: 		UIButton!
	
	@IBOutlet weak var googleMapView: 		GMSMapView!
	

	var gooogleMapViewRef: 	GMSMapView?
	var userLat: 			Double?
	var userLon:			Double?
	var storeArraySize: 	Int = -1
	
	var stores			= [Store]()
	let locationManager = CLLocationManager()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.requestPointsForLocation()
		self.reloadUserLocation()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	

	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		// Get the new view controller using segue.destinationViewController.
		// Pass the selected object to the new view controller.
	}
	*/
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "MainViewSegue" {
			let MainVC	= segue.destination as! MainViewController
			MainVC.stores 			= self.stores
			MainVC.storeArraySize 	= self.storeArraySize
		}
	}
	
	@IBAction func nextView(_ sender: UIButton) {
		if self.userLat != nil && self.userLon != nil {
			self.getApiData()
		} else {
			self.sentToLocalizationSystem()
		}
	}
	
	@objc func nextLocation(_ sender: UIBarButtonItem?) {
//		if self.currentDestination == nil {
//			self.currentDestination = destionations.first
//			self.mapView?.camera = GMSCameraPosition.camera(withTarget: self.currentDestination!.location, zoom: self.currentDestination!.zoom)
//		}
		//		buenavista CC 11.014032, -74.827933
//		let nextLocation = CLLocationCoordinate2D(latitude: 11.014032, longitude: -74.827933)
//		self.mapView?.camera = GMSCameraPosition.camera(withLatitude: nextLocation.latitude, longitude: nextLocation.longitude, zoom: 13)
		
//		let marker = GMSMarker(position: self.currentDestination!.location)
//		marker.title = self.currentDestination?.name
		//		marker.snippet = "Msj prueba"
//		marker.map = self.mapView
	}
	
	func requestPointsForLocation() {
		self.locationManager.delegate = self
		self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
		self.locationManager.requestAlwaysAuthorization() // TAMBIÉN SOLICITA EN SEGUNDO PLANO LA LOCALIZACIÓN
		
		if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
			self.locationManager.startUpdatingLocation()
		}
	}
	
	func getCountryAndCityByCLLocation(location: CLLocation, completion: @escaping (String, String) -> ()) {
		CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
			if let error = error {
				print(error)
			} else if let country = placemarks?.first?.country,
				let city = placemarks?.first?.locality {
				completion(country, city)
			}
		}
	}

	func sentToLocalizationSystem() {
		let locationAlert = UIAlertController(title: "Alerta", message: "Debes habilitar la localización", preferredStyle: UIAlertControllerStyle.alert)
		
		locationAlert.addAction(UIAlertAction(title: "Habilitar", style: .default, handler: { (action: UIAlertAction!) in
			guard let systemSettingsForApp = URL(string: UIApplicationOpenSettingsURLString) else {
				return
			}
			if UIApplication.shared.canOpenURL(systemSettingsForApp) {
				UIApplication.shared.open(systemSettingsForApp, completionHandler: { (success) in
				})
			}
		}))
		
		locationAlert.addAction(UIAlertAction(title: "cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
		}))
		self.present(locationAlert, animated: true, completion: nil)
	}
	
	func reloadUserLocation() {
		//		11.022487, -74.815309 Carrera 72 # 91a - 100 - My Home
		//		11.014021, -74.827976 C.C. Buenavista
		//		4.652390, -74.080355 Bogotá
		var camera	= GMSCameraPosition.camera(withLatitude: 4.652390, longitude: -74.080355, zoom: 10)
		if self.userLat != nil && self.userLon != nil {
			let location	= CLLocation(latitude: self.userLat!, longitude: self.userLon!)
			self.getCountryAndCityByCLLocation(location: location) { country, city in
				self.lblCityReference.text		= city
				self.lblCountryReference.text	= country
				//print("distancia entre mi casa y buenavista = \(location.distance(from: CLLocation(latitude: 11.014021, longitude: -74.827976)))")
			}
			camera = GMSCameraPosition.camera(withLatitude: self.userLat!, longitude: self.userLon!, zoom: 14)
			
			let pointMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: self.userLat!, longitude: self.userLon!))
//			let pointMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 4.697203, longitude: -74.087541)) // Villa Carolina Bogotá 4.697203, -74.087541
			pointMarker.title = "Tu locación"
			pointMarker.map = self.googleMapView
		}
		
		self.googleMapView.camera	= camera
	}
	
	func getApiData() {
			let loading = UIAlertController.loadingWith(message: "Espere por favor....")
			self.present(loading, animated: true, completion: {
				Request().expectingObjects(endpoint: .stores, method: "GET", params: "", token: "",
					completion: { (data, code, err) in
						loading.dismiss(animated: true, completion: {
							if err != nil {
								if code == 3 {
									UIAlertController.showAlertWith(message: "Verifica tu conexión a internet", title: "", in: self, completion: nil)
								}else{
									UIAlertController.showAlertWith(message: "Se generó un error procesando la petición, intente nuevamente", title: "", in: self, completion: nil)
									LogsRecord().add(Log(
										code: code!,
										desc: err!,
										origin: "Petición http hacia \(Endpoints.stores)",
										userinfo: "reference")
									)
									LogsRecord().sendToServer()
								}
								print(err!)
							} else {
								if code != 200 {
									switch code! {
									case 404:
										UIAlertController.showAlertWith(message: "Ruta no encontrada", title: "", in: self, completion: nil)
										break
									case 500:
										UIAlertController.showAlertWith(message: "Error del servidor, intente más tarde", title: "", in: self, completion: nil)
										break
									default:
										UIAlertController.showAlertWith(message: "Intente más tarde", title: "", in: self, completion: nil)
										break
									}
								}else{
									if data!.count > 0 {
										let location	= CLLocation(latitude: self.userLat!, longitude: self.userLon!)
//										let location	= CLLocation(latitude: 4.697203, longitude: -74.087541) // Villa Carolina Bogotá 4.697203, -74.087541
										var countArray	= 0;
//										self.stores.removeAll()
										for index in 0..<data!.count {
											let locationRef		= (data![index].value(forKey: "ubicacion_txt") as! String).count > 0 ? (data![index].value(forKey: "ubicacion_txt") as! String).components(separatedBy: ",") : [""]
											if locationRef.count == 2 {
												if location.distance(from: CLLocation(latitude: Double(locationRef[0])!, longitude: Double(locationRef[1])!)) < 6000 {
													countArray+=1
													let deliveryRef 	= Int((data![index].value(forKey: "domicilio") as! String).replacingOccurrences(of: ".00", with: "")) as! Int
													let ratingRef		= Int((data![index].value(forKey: "rating") as! String).replacingOccurrences(of: ".00", with: "")) as! Int
													let deliveryTimeRef	= Int((data![index].value(forKey: "tiempo_domicilio") as! String).replacingOccurrences(of: ".00", with: "")) as! Int

													self.stores.append(Store(
														category: data![index].value(forKey: "categorias") as! String,
														delivery: deliveryRef,
														url_detail: data![index].value(forKey: "url_detalle") as! String,
														logo_path: data![index].value(forKey: "logo_path") as! String,
														store_name: data![index].value(forKey: "nombre") as! String,
														rating: Int(data![index].value(forKey: "rating") as! String) as! Int,
														delivery_time: Int(data![index].value(forKey: "tiempo_domicilio") as! String) as! Int,
														lat: locationRef[0],
														long: locationRef[1])
													)
												}
										}	}
										self.storeArraySize = countArray
										
										if self.storeArraySize > 0 {
											self.performSegue(withIdentifier: "MainViewSegue", sender: nil)
										} else {
											UIAlertController.showAlertWith(message: "No hay restaurantes disponibles para tu locación", title: "", in: self, completion: nil)
										}
									}
								}
							}
							// self.requestPointsForLocation()
						})
					}
				)
			})
		}

}

extension LobbyViewController: CLLocationManagerDelegate {
	
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		if status == .authorizedAlways || status == .authorizedWhenInUse {
			manager.startUpdatingLocation()
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let location = locations.sorted(by: { $0.horizontalAccuracy < $1.horizontalAccuracy}).first!
		let lon = "\(location.coordinate.longitude)"
		let lat = "\(location.coordinate.latitude)"
//		print("Ubicación actual: \(lat),\(lon)")
		if self.userLat == nil {
			self.userLat = location.coordinate.latitude
			self.userLon = location.coordinate.longitude
			self.reloadUserLocation()
		}
		manager.stopUpdatingLocation()
	}
}
