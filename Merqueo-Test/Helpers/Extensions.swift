//
//  Extensions.swift
//  Merqueo-Test
//
//  Created by Miguel Angel Valencia on 10/29/17.
//  Copyright © 2017 Miguel Angel Valencia. All rights reserved.
//

import SystemConfiguration
import Foundation
import UIKit

extension Int {
	
	func decimalFormatted() -> String {
		let fmt = NumberFormatter()
		fmt.groupingSeparator = ","
		fmt.numberStyle = .decimal
		return fmt.string(from: NSNumber(value: self))!
	}
	
}

extension UIAlertController {
	
	static func showAlertWith(message: String , title: String ,in viewController : UIViewController, completion: (() -> Void)?) {
		let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel) { action -> Void in
			if completion != nil {
				completion!()
			}
		}
		actionSheetController.addAction(cancelAction)
		viewController.present(actionSheetController, animated: true, completion: nil)
	}
	
	static func loadingWith(message: String!) -> UIAlertController {
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		
		let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
		loadingIndicator.hidesWhenStopped = true
		loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
		loadingIndicator.startAnimating();
		
		alert.view.addSubview(loadingIndicator)
		return alert
	}
	
}

extension Date {
	
	func formatToServerDate() -> String {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-mm-dd'T'HH:MM:ss"
		return formatter.string(from: self)
	}
	
}

public class InternetAccess {
	
	func isConnectedToNetwork() -> Bool {
		guard let flags = getFlags() else { return false }
		let isReachable = flags.contains(.reachable)
		let needsConnection = flags.contains(.connectionRequired)
		return (isReachable && !needsConnection)
	}
	
	func getFlags() -> SCNetworkReachabilityFlags? {
		guard let reachability = ipv4Reachability() ?? ipv6Reachability() else {
			return nil
		}
		var flags = SCNetworkReachabilityFlags()
		if !SCNetworkReachabilityGetFlags(reachability, &flags) {
			return nil
		}
		return flags
	}
	
	func ipv6Reachability() -> SCNetworkReachability? {
		var zeroAddress = sockaddr_in6()
		zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
		zeroAddress.sin6_family = sa_family_t(AF_INET6)
		
		return withUnsafePointer(to: &zeroAddress, {
			$0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
				SCNetworkReachabilityCreateWithAddress(nil, $0)
			}
		})
	}
	
	func ipv4Reachability() -> SCNetworkReachability? {
		var zeroAddress = sockaddr_in()
		zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
		zeroAddress.sin_family = sa_family_t(AF_INET)
		
		return withUnsafePointer(to: &zeroAddress, {
			$0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
				SCNetworkReachabilityCreateWithAddress(nil, $0)
			}
		})
	}
	
	/*class func isConnectedToNetwork()->Bool{
	
	var Status:Bool = false
	let url = NSURL(string: "http://google.com/")
	let request = NSMutableURLRequest(url: url! as URL)
	request.httpMethod = "HEAD"
	request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
	request.timeoutInterval = 3.0
	
	var response: URLResponse?
	do {
	var data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as NSData?
	
	if let httpResponse = response as? HTTPURLResponse {
	if httpResponse.statusCode == 200 {
	Status = true
	}
	}
	} catch{
	Status = false
	}
	
	
	return Status
	}*/
}

public enum Model : String {
	case simulator = "simulator/sandbox",
	iPod1          = "iPod 1",
	iPod2          = "iPod 2",
	iPod3          = "iPod 3",
	iPod4          = "iPod 4",
	iPod5          = "iPod 5",
	iPad2          = "iPad 2",
	iPad3          = "iPad 3",
	iPad4          = "iPad 4",
	iPhone4        = "iPhone 4",
	iPhone4S       = "iPhone 4S",
	iPhone5        = "iPhone 5",
	iPhone5S       = "iPhone 5S",
	iPhone5C       = "iPhone 5C",
	iPadMini1      = "iPad Mini 1",
	iPadMini2      = "iPad Mini 2",
	iPadMini3      = "iPad Mini 3",
	iPadAir1       = "iPad Air 1",
	iPadAir2       = "iPad Air 2",
	iPhone6        = "iPhone 6",
	iPhone6plus    = "iPhone 6 Plus",
	iPhone6S       = "iPhone 6S",
	iPhone6Splus   = "iPhone 6S Plus",
	iPhoneSE       = "iPhone SE",
	iPhone7        = "iPhone 7",
	iPhone7plus    = "iPhone 7 Plus",
	unrecognized   = "?unrecognized?"
}

public extension UIDevice {
	public var type: Model {
		var systemInfo = utsname()
		uname(&systemInfo)
		let modelCode = withUnsafePointer(to: &systemInfo.machine) {
			$0.withMemoryRebound(to: CChar.self, capacity: 1) {
				ptr in String.init(validatingUTF8: ptr)
				
			}
		}
		var modelMap : [ String : Model ] = [
			"i386"      : .simulator,
			"x86_64"    : .simulator,
			"iPod1,1"   : .iPod1,
			"iPod2,1"   : .iPod2,
			"iPod3,1"   : .iPod3,
			"iPod4,1"   : .iPod4,
			"iPod5,1"   : .iPod5,
			"iPad2,1"   : .iPad2,
			"iPad2,2"   : .iPad2,
			"iPad2,3"   : .iPad2,
			"iPad2,4"   : .iPad2,
			"iPad2,5"   : .iPadMini1,
			"iPad2,6"   : .iPadMini1,
			"iPad2,7"   : .iPadMini1,
			"iPhone3,1" : .iPhone4,
			"iPhone3,2" : .iPhone4,
			"iPhone3,3" : .iPhone4,
			"iPhone4,1" : .iPhone4S,
			"iPhone5,1" : .iPhone5,
			"iPhone5,2" : .iPhone5,
			"iPhone5,3" : .iPhone5C,
			"iPhone5,4" : .iPhone5C,
			"iPad3,1"   : .iPad3,
			"iPad3,2"   : .iPad3,
			"iPad3,3"   : .iPad3,
			"iPad3,4"   : .iPad4,
			"iPad3,5"   : .iPad4,
			"iPad3,6"   : .iPad4,
			"iPhone6,1" : .iPhone5S,
			"iPhone6,2" : .iPhone5S,
			"iPad4,1"   : .iPadAir1,
			"iPad4,2"   : .iPadAir2,
			"iPad4,4"   : .iPadMini2,
			"iPad4,5"   : .iPadMini2,
			"iPad4,6"   : .iPadMini2,
			"iPad4,7"   : .iPadMini3,
			"iPad4,8"   : .iPadMini3,
			"iPad4,9"   : .iPadMini3,
			"iPhone7,1" : .iPhone6plus,
			"iPhone7,2" : .iPhone6,
			"iPhone8,1" : .iPhone6S,
			"iPhone8,2" : .iPhone6Splus,
			"iPhone8,4" : .iPhoneSE,
			"iPhone9,1" : .iPhone7,
			"iPhone9,2" : .iPhone7plus,
			"iPhone9,3" : .iPhone7,
			"iPhone9,4" : .iPhone7plus,
			]
		
		if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
			return model
		}
		return Model.unrecognized
	}
}
